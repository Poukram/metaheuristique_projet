function TestSuite(base_param, pb_names, alg_names)
%TESTSUITE Summary of this function goes here
%   Detailed explanation goes here
algnb = numel(alg_names);
pbnb = numel(pb_names);
numtests = 10;
pb = cell(1,numel(pb_names));

for pn = 1:pbnb
    pb{pn} = GenericProblem(pb_names(pn));
end
res = cell(algnb, numtests, pbnb);

% Progress monitoring
q = parallel.pool.DataQueue;
afterEach(q, @disp);

% DATA COLLECTION ( use parfor here)
timetot = tic;

parfor p = 1:pbnb
    param = base_param;
    param.pb = pb{p};
    
    send(q, "Started " + param.pb.name + " tests!");
    
    for a = 1:algnb
        param.algo_name = alg_names(a);
        if (param.algo_name == "PAES")
            param.g_max = param.g_max * 100;
        end
        algres = cell(1, numtests);
        
        for t = 1:numtests
            send(q, "Starting " + param.algo_name + " " + param.pb.name + ...
                " test nb : " + t);
            time = tic;
            result = MOGACaller(param);
            time = toc(time);
            algres{1, t} = {result(end); time};
            
            send(q, "Finished " + param.algo_name + " " + param.pb.name + ...
                " test nb : " + t + " in " + time + " seconds");
        end
        res(a,:,p) = algres;
    end
    send(q, "Finished all " + param.pb.name + " tests!");
end

timetot = toc(timetot);
disp("Finished doing the tests in " + timetot + " seconds");

% DATA PROCESSING
disp("Starting processing results!")
time = tic;

proc_res(numel(alg_names), pbnb, 3) = 0;
bpfs_res = cell(algnb, pbnb);

for a = 1:algnb
    for p = 1:pbnb
        tmp = [res{a, :, p}];
        pfs = [tmp{1,:}];
        tmpres(numel(pfs) - 1) = 0;
        pfsres(numel(pfs)) = 0;
        
        mtime = mean([tmp{2,:}]);
        stime = std([tmp{2,:}]);
        
        proc_res(a, p, 1) = mtime;
        proc_res(a, p, 2) = stime;
        
        for i = 1:numel(pfs)
            tmpidx = 1;
            for j = 1:numel(pfs)
                if (i == j)
                    continue;
                end
                
                if (numel(pfs{i}) == 0)
                    tmpres(tmpidx) = 0;
                elseif (numel(pfs{j}) == 0)
                    tmpres(tmpidx) = 1;
                else
                    tmpres(tmpidx) = ComparePFs([pfs(i) pfs(j)], pb{p}.objectives_nb);
                end
                tmpidx = tmpidx + 1;
            end
            pfsres(i) = mean(tmpres);
        end
        [~, bpfidx] = max(pfsres);
        bpfs_res(a, p) = pfs(bpfidx);
    end
end

for p = 1:pbnb
    pf1 = bpfs_res(1, p);
    pf2 = bpfs_res(2, p);
    if (numel(pf1{1}) == 0 || numel(pf2{1}) == 0)
        if (numel(pf1{1}) == 0 && numel(pf2{1}) == 0)
            proc_res(1, p, 3) = -1;
            proc_res(1, p, 3) = -1;
            continue;
        elseif (numel(pf1{1}) == 0)
            proc_res(1, p, 3) = 0;
            proc_res(2, p, 3) = 1;
            continue;
        elseif (numel(pf2{1}) == 0)
            proc_res(1, p, 3) = 1;
            proc_res(2, p, 3) = 0;
            continue;
        end
    else
        proc_res(1, p, 3) = ComparePFs([pf1 pf2], pb{p}.objectives_nb);
        proc_res(2, p, 3) = ComparePFs([pf2 pf1], pb{p}.objectives_nb);
    end
end

defin_res( 3 * algnb, pbnb) = 0;
for i = 1:3
    for j = 1:algnb
        for k = 1:pbnb
            defin_res(i + 3*(j-1), k) = proc_res(j, k, i);
        end
    end
end

time = toc(time);
disp("Finished processing results in " + time + " seconds")
% TODO save to EXCEL file
names = vertcat(repmat("MOPSO",3,1), repmat("PAES",3,1));
mesures = repmat(["Temps moyen";"Variance temps";"Taux de domination"], 2, 1);
excell_file = "test_data.xlsx";
xlswrite(excell_file, [["Algorithme" "Mesures" pb_names]; [names mesures string(defin_res)]], "Pareto front optimization", "A1");