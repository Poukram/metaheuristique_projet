classdef NSGA2_Ind < Individual
    %NSGA2_IND Summary of this class goes here
    %   Detailed explanation goes here
    
    properties       
        rank
        crowd_dist
        S
        n
    end
    
    methods
        function obj = NSGA2_Ind(vars, objs_vals)
            %NSGA2_IND Construct an instance of this class
            %   Detailed explanation goes here
            if (nargin == 0)
                vars = 0;
                objs_vals = 0;
            end
            obj = obj@Individual(vars, objs_vals);
            obj.rank = 0;
            obj.crowd_dist = 0;
            obj.S = {};
            obj.n = 0;
        end
    end
    methods(Access = protected)
        function cp = copyElement(obj)
            cp = NSGA2_Ind;
            cp.rank = obj.rank;
            cp.crowd_dist = obj.crowd_dist;
            cp.S = [];
            cp.n = obj.n;
            cp.vars = obj.vars;
            cp.objs_vals = obj.objs_vals;
        end
    end
    methods(Static)
        function obj_arr = GetNewArray(dim)
            obj_arr(dim) = NSGA2_Ind;
            for i = 1:dim
                obj_arr(i) = NSGA2_Ind;
            end
        end
    end
end

