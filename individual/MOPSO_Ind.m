classdef MOPSO_Ind < Individual
    %INDIVIDUAL Stores information about an individual to be used later for
    %data analysis
    
    properties
        speed;
        dominated;
        hcidx;
        bvars;
        bobjsvals;
    end
    
    methods
        function obj = MOPSO_Ind(vars, objs_vals)
            %INDIVIDUAL Construct an instance of this class
            %   Detailed explanation goes here
            if (nargin == 0)
                vars = 0;
                objs_vals = 0;
            end
            obj = obj@Individual(vars, objs_vals);
            obj.speed = zeros(1,vars);
            obj.dominated = false;
            obj.hcidx = 0;
            obj.bvars = 0;
            obj.bobjsvals = 0;
        end
    end
    methods(Static)
        function obj_vec = GetNewVect(len, vars, objs_vals)
            obj_vec(len) = MOPSO_Ind(vars, objs_vals);
            for i = 1:len
                obj_vec(i) = MOPSO_Ind(vars, objs_vals);
            end
        end
    end
end

