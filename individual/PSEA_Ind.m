% FIXME use and edit as needed
classdef PSEA_Ind < matlab.mixin.Copyable & matlab.mixin.Heterogeneous
    %INDIVIDUAL Stores information about an individual to be used later for
    %data analysis
    
    properties
        vars
        objs_vals
    end
    
    methods
        function obj = Individual(vars, objs_vals)
            %INDIVIDUAL Construct an instance of this class
            %   Detailed explanation goes here
            if (nargin == 0)
                vars = 0;
                objs_vals = 0;
            end
            obj.vars = zeros(1,vars);
            obj.objs_vals = zeros(1,objs_vals);
        end
    end
end

