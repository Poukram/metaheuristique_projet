%------------Configuring the path and initializing -------------------
clear variables
close all
clc
addpath(genpath(pwd))

pb_names = ["FON", "KUR", "POL", "SCH", "ZDT1", "ZDT2", "ZDT3", "ZDT4", "ZDT6"];
algo_names = ["MOPSO", "PAES"];
%---------------------------------------------------------------------

param.popsize = 120;
param.archsize = 100;
param.g_max = 120; % The maximum number of generations
param.pc = 0.75; % The probability of a crossover to happen
param.pm = 0.5; % The probability of a mutation to happen
param.nbdiv = 30;

%------------Call to the genetic algorithm-----------
TestSuite(param, pb_names, algo_names);
