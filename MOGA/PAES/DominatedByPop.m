function res = DominatedByPop(Ind_ov, Pop, objnb)
%DOMINATEDBYPOP True if individual dominated false otherwise
res = false;
for s = Pop
   if Dominates(s.objs_vals, Ind_ov, objnb)
       res = true;
       return;
   end
end
end

