function res = PAES(param)
%PAES Summary of this function goes here
%   Detailed explanation goes here
res = cell(1,param.g_max);
C = Individual(param.pb.vars_nb, param.pb.objectives_nb);
C.vars = param.pb.GenRndVars();
EvalPop(C, param.pb);
A = Archive(param.archsize, param.pb, param.nbdiv);
A.addsol(copy(C));

for i = 1:param.g_max
    res{i} = copy(A.solutions);
    
    EvalPop(C, param.pb);
    M = PAESNormalMutation(C, param.pm, param.pb, i, param.g_max);
    EvalPop(M, param.pb);
    if Dominates(C.objs_vals, M.objs_vals, param.pb.objectives_nb)
        continue;
    elseif Dominates(M.objs_vals, C.objs_vals, param.pb.objectives_nb)
        C = M;
        A.addsol(copy(M));
    elseif DominatedByPop(M.objs_vals, A.solutions, param.pb.objectives_nb)
        continue;
    else
        if A.addsol(copy(M))
            C = PAESTest(C,M,A);
        end
    end
end
res{end} = copy(A.solutions);
end

