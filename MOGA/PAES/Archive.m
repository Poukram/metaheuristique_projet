classdef Archive < handle
    %PAESARCHIVE Summary of this class goes here
    %   Detailed explanation goes here
    properties
        capacity
        pb
        numdiv
        minimum
        maximum
        indcnt
        solutions
    end
    
    methods
        function obj = Archive(capacity, pb, numdiv)
            %PAESARCHIVE Construct an instance of this class
            %   Detailed explanation goes here
            obj.capacity = capacity;
            obj.pb = pb;
            obj.numdiv = numdiv;
            obj.minimum = zeros(1, pb.objectives_nb);
            obj.maximum = zeros(1, pb.objectives_nb);
            obj.indcnt = zeros(1, obj.numdiv^pb.objectives_nb);
        end
        
        function adaptgrid(obj)
            %METHOD1 Summary of this method goes here
            %   Detailed explanation goes here
            obj.minimum(1:end) = Inf;
            obj.maximum(1:end) = -Inf;
            obj.indcnt(1:end) = 0;
            
            for s = obj.solutions
                for i = 1:obj.pb.objectives_nb
                    obj.minimum(i) = min(obj.minimum(i), s.objs_vals(i));
                    obj.maximum(i) = max(obj.maximum(i), s.objs_vals(i));
                end
            end
            
            for s = obj.solutions
                idx = obj.findindex(s);
                obj.indcnt(idx) = obj.indcnt(idx) + 1;
            end
        end
        
        function idx = findindex(obj, solution)
            idx = 0;
            for i = 1:obj.pb.objectives_nb
                value = solution.objs_vals(i);
                if (value < obj.minimum(i)) || (value > obj.maximum(i))
                    idx = -1;
                    return;
                else
                    if obj.maximum(i) == obj.minimum(i)
                        tmpIdx = 0;
                    else
                        tmpIdx = floor(obj.numdiv * ((value - obj.minimum(i))/...
                            (obj.maximum(i) - obj.minimum(i))));
                    end
                    if (tmpIdx == obj.numdiv)
                        tmpIdx = tmpIdx - 1;
                    end
                    idx = idx + tmpIdx*(obj.numdiv^(i-1));
                end
            end
            idx = idx + 1; % because matlab indexes starting from 1
        end
        
        function res = addsol(obj, solution)
            tokeep = true(1,numel(obj.solutions));
            go_on = true;
            
            for s = 1:numel(obj.solutions)
                if Dominates(solution.objs_vals, obj.solutions(s).objs_vals, obj.pb.objectives_nb)
                    tokeep(s) = false;
                elseif Dominates(obj.solutions(s).objs_vals, solution.objs_vals, obj.pb.objectives_nb)
                    go_on = false;
                    break;
                end
            end
            
            for i = 1:numel(tokeep)
                if obj.solutions(i) == false
                    cIdx = obj.findindex(obj.solutions(i));
                    obj.indcnt(cIdx) = obj.indcnt(cIdx) - 1;
                    obj.solutions(i) = [];
                end
            end
            
            if ~go_on
                res = false;
                return;
            end
            
            if numel(obj.solutions) == 0
                obj.solutions = [obj.solutions solution];
                obj.adaptgrid();
                res = true;
                return;
            end
            
            obj.solutions = [obj.solutions solution];
            idx = obj.findindex(solution);
            
            if (idx < 0)
                obj.adaptgrid();
                idx = obj.findindex(solution);
            else
                obj.indcnt(idx) = obj.indcnt(idx) + 1;
            end
            
            if numel(obj.solutions) < obj.capacity
                res = true;
                return;
            elseif obj.indcnt(idx) == obj.indcnt(obj.findDensestCell())
                cIdx = obj.findindex(obj.solutions(end));
                obj.indcnt(cIdx) = obj.indcnt(cIdx) - 1;
                obj.solutions(end) = [];
                res = false;
                return;
            else
                [cIdx, sIdx] = obj.rndSolFromDensestCell();
                obj.indcnt(cIdx) = obj.indcnt(cIdx) - 1;
                obj.solutions(sIdx) = [];
                res = true;
                return;
            end
        end
        
        function idx = findDensestCell(obj)
            [~,idx] = max(obj.indcnt);
        end
        
        function [cIdx, idx] = rndSolFromDensestCell(obj)
            cIdx = obj.findDensestCell();
            idx = -1;
            cellsols(obj.indcnt(cIdx)) = 0;
            solcnt = 1;
            
            for s = 1:numel(obj.solutions)
                tmpIdx = obj.findindex(obj.solutions(s));
                if tmpIdx == cIdx
                    cellsols(solcnt) = s;
                    solcnt = solcnt + 1;
                end
            end
            idx = randsample(cellsols, 1);
        end
    end
end

