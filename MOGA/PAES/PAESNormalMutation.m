function Ind = PAESNormalMutation(IndOrig, pm, pb, g, gmax)
%NORMALMU Custom mutation operator based on normal mutation operator 
%   Uses var range and integrated correction for out of bounds results

Ind = copy(IndOrig);
shrinkf = (0.4*((gmax - g + 1) / gmax)^2.25 + 0.05);
var = randsample(pb.vars_nb, floor(rand() * pb.vars_nb) + 1);

for k = 1:numel(var)
    i = var(k);
    if rand() < pm
        sigma = abs(pb.vars_range(i,2) - pb.vars_range(i,1)) * shrinkf;
        Ind.vars(i) = Ind.vars(i) + sigma * normrnd(0,1);
        
        if Ind.vars(i) > pb.vars_range(i,2)
            Ind.vars(i) =  pb.vars_range(i,2);
        elseif Ind.vars(i) < pb.vars_range(i,1)
            Ind.vars(i) = pb.vars_range(i,1);
        end
    end
end
end

