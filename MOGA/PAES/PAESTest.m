function res = PAESTest(C, M, A)
%PAESTEST Summary of this function goes here
%   Detailed explanation goes here
    C_cidx = A.findindex(C);
    M_cidx = A.findindex(M);
    
    if C_cidx < 0
        res = M;
        return;
    elseif M_cidx < 0
        res = C;
        return;
    elseif A.indcnt(C_cidx) > A.indcnt(M_cidx)
        res = M;
        return;
    else
        res = C;
        return;
    end
end

