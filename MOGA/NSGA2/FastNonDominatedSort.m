function [res, Pop] = FastNonDominatedSort(Pop, pb)
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here
Fronts = {};
Fronts{1} = {};
i = 1;

for p = Pop
    p.S = [];
    p.n =0;
    for q = Pop
        if (q == p)
            continue;
        end
        % if p dominates q
        if (Dominates(p.objs_vals, q.objs_vals, pb.objectives_nb) == 1)
            p.S = [p.S q];
        % if q dominates p
        elseif (Dominates(q.objs_vals, p.objs_vals, pb.objectives_nb) == 1)
            p.n = p.n + 1;
        end
    end
    
    if (p.n == 0)
        p.rank = 1;
        Fronts{1} = [Fronts{1} p];
    end
end

while (size(Fronts{i,:}, 2) ~= 0)
    Q = {};
    for p = Fronts{i,:}
        for q = p.S
            q.n = q.n - 1;
            if (q.n == 0)
                q.rank = i + 1;
                Q = [Q q];
            end
        end
    end
    i = i +1;
    Fronts{i,:} = Q;
end
res = Fronts(1:end-1);
end

