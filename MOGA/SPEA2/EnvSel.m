function res = EnvSel(Pop, popsize, archsize, pb)
%ENVSEL Summary of this function goes here
%   Detailed explanation goes here
new_archive = [];

k = 0;

for i = Pop
    if (i.fitness < 1)
        k = k + 1;
        new_archive = [new_archive, copy(i)];
    end
end

% New archive is too small
if (k < archsize)
    % FIXME
    subset = [];
    for i = Pop
        if (i.fitness >= 1)
            subset = [subset, i];
        end
    end
    [~, idx] = sort([subset.fitness]);
    subset = subset(idx);
    
    for i = k+1:archsize
        new_archive = [new_archive, copy(subset(i - k))];
    end
    
% New archive is too big
elseif (k > archsize)
    while (k ~= archsize)
        d = zeros(k,k);
        to_rm = [];
        to_exam = 1:k;
        
        for i = 1:k
            x = new_archive(i);
            for j = 1:k
                y = new_archive(j);
                d(i,j) = sqrt(sum((x.objs_vals - y.objs_vals).^2));
            end
            d(i,:) = sort(d(i,:));
        end
        
        
        for kk = 1:k
            for i = to_exam
                removable = 1;
                for j = to_exam
                    if d(i,kk) > d(j,kk)
                        removable = 0;
                        break;
                    end
                end
                if (removable == 1)
                    to_rm = [to_rm, i];
                end
            end
            if numel(to_rm) == 1
                break;
            elseif numel(to_rm) > 1
                if (kk == k)
                    break;
                end
                to_exam = to_rm;
                to_rm = [];
            else
                warning('Unexpected behavior')
            end
        end
        new_archive(to_rm(1)) = [];
        k = k -1;
    end
end

res = new_archive(1:archsize);
end

