function pareto_front = SPEA2(param)
%SPEA2 Summary of this function goes here
%   must pass population size, archive size and max generations number
pareto_front(param.archsize) = SPEA2_Ind;
P = InitSPEA2Pop(param.popsize, param.pb);
EvalPop(P, param.pb);
A = []; % Archive set

for g = 1:param.g_max
    U = [P, A];
    usize = numel(U);
    FitnessCompute(U, usize, param.pb);
    A = EnvSel(U, usize, param.archsize, param.pb);
    P = MakeNewPop(A, param.popsize, param.pc, param.pm, param.pb, param.algo_name);
    
    pareto_front = copy(A);
end
end

