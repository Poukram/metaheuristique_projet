function NewPop = MakeNewPop(Pop, popsize, pc, pm, pb, MOEA)
%MAKENEWPOP Summary of this function goes here
%   Detailed explanation goes here

switch MOEA
    case 'NSGA2'
        MatingPool = NSGA2BinaryT(Pop, popsize);
        Children = NSGA2LaplaceCrossover(MatingPool, popsize, pc, pb);
    case 'SPEA2'
        MatingPool = SPEA2BinaryT(Pop, popsize);
        Children = SPEA2LaplaceCrossover(MatingPool, popsize, pc, pb);
    otherwise
        Children = [];
        warning('Unknown algorithm, please check your code');
end

Mutants = NormalMutation(Children, popsize, pm, pb);
NewPop = ValidityCheckT1(Mutants, popsize, pb);
EvalPop(NewPop, pb);
end

