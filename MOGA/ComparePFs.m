function res = ComparePFs(origpfs, nbobjs)
%COMPAREPFS Compares two pareto fronts
%   Returns the dominance proportion of pf1 over pf2, if they're not of the
%   same size, a random sampling occurs on the bigger one.
if numel(origpfs) ~= 2
    warning("Incorrect number of pareto fronts provided : " + numel(pfs))
end

pfs = origpfs;
sz = [numel(origpfs{1}) numel(origpfs{2})];
[MA, MAidx] = max(sz);
[mi, miidx] = min(sz);
diffsz = MA ~= mi;
numrep = 1;

if (diffsz)
    szma = numel(origpfs{MAidx});
    szmi = numel(origpfs{miidx});
    numrep = floor(20 * (szma - szmi)/szma) + 1;
end
tmpres(numrep) = 0;

for k = 1:numrep
    if diffsz
        smpl = randsample(szma, szmi);
        tmp = origpfs{MAidx};
        pfs{MAidx} = tmp(smpl);
    end
    
    pf1 = pfs{1};
    pf2 = pfs{2};
    pf1res = zeros(1,numel(pf1));
    
    for i1 = 1:numel(pf1)
        for i2 = 1:numel(pf2)
            if Dominates(pf1(i1).objs_vals, pf2(i2).objs_vals, nbobjs)
                pf1res(i1) = pf1res(i1) + 1;
            end
        end
    end
    tmpres(k) = mean(pf1res) / numel(pf1);
end

res = mean(tmpres);
end

