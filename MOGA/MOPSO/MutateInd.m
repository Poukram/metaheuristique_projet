function MutateInd(Ind, pm, pb)
%MUTATEIND Summary of this function goes here
%   Detailed explanation goes here
var = randsample(pb.vars_nb, floor(rand() * pb.vars_nb) + 1);

for k = 1:numel(var)
    dim = var(k);
    if rand() < pm
        range = abs(pb.vars_range(dim, 2) - pb.vars_range(dim, 1)) * pm;
        ub = Ind.vars(dim) + range;
        lb = Ind.vars(dim) - range;
        
        if (ub > pb.vars_range(dim, 2))
            ub = pb.vars_range(dim, 2);
        end
        
        if (lb < pb.vars_range(dim, 1))
            lb = pb.vars_range(dim, 1);
        end
        
        Ind.vars(dim) = abs(ub - lb) * rand() + lb;
    end
end
end

