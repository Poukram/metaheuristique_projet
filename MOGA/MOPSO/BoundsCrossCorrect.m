function BoundsCrossCorrect(Ind, pb)
%BOUNDSCROSSCORRECT Summary of this function goes here

for i = 1:pb.vars_nb
    if (Ind.vars(i) > pb.vars_range(i, 2))
        Ind.vars(i) = pb.vars_range(i, 2);
        Ind.speed(i) = - Ind.speed(i);
    end
    if (Ind.vars(i) < pb.vars_range(i, 1))
        Ind.vars(i) = pb.vars_range(i, 1);
        Ind.speed(i) = - Ind.speed(i);
    end
end
end

