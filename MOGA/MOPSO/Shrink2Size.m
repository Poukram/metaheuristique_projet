function REP = Shrink2Size(REP, HCubes, nb2rm)
%SHRINK2SIZE Summary of this function goes here
%   Detailed explanation goes here

for i = 1:nb2rm
    [~, hcidx] = sort([HCubes.indcnt],'descend');
    tmp = find([REP.hcidx] == hcidx(1)); % we take all particles in this HC
    sel =  tmp(randsample(numel(tmp),1));
    REP(sel) = [];
    HCubes.indcnt(hcidx(1)) = HCubes.indcnt(hcidx(1)) - 1;
    
    if (HCubes.indcnt(hcidx(1)) == 1)
        HCubes.fitness(hcidx(1)) = 1;
    elseif (HCubes.indcnt(hcidx(1)) > 1)
        HCubes.fitness(hcidx(1)) = 10/HCubes.indcnt(hcidx(1));
    elseif (HCubes.indcnt(hcidx(1)) == 0)
         HCubes.fitness(hcidx(1)) = 0;
    end
end
end

