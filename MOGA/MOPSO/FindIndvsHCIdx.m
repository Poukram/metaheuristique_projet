function FindIndvsHCIdx(Pop, HCubes, pb)
%FINDINDHCIDX Summary of this function goes here
%   Detailed explanation goes here
for p = Pop
    hcidx = zeros(1, pb.objectives_nb);
    for i = 1:pb.objectives_nb
        for k = 1:HCubes.numpts
            if ( p.objs_vals(i) >= HCubes.bounds(i,1, k) &&...
                    p.objs_vals(i) <= HCubes.bounds(i,2, k))
                hcidx(i) = k;
                break;
            end
        end
    end
    % FIXME works only with 2 objectives pbs
    p.hcidx = hcidx(2) + (hcidx(1)-1)*HCubes.numpts;
    HCubes.indcnt(p.hcidx) = HCubes.indcnt(p.hcidx) + 1;
end

for i = 1:numel(HCubes.indcnt)
    if (HCubes.indcnt(i) == 1)
        HCubes.fitness(i) = 1;
    elseif (HCubes.indcnt(i) > 1)
        HCubes.fitness(i) = 10/HCubes.indcnt(i);
    end
end
end

