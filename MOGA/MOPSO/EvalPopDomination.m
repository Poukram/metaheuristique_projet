function EvalPopDomination(Pop, popsize, pb)
%EVALPOPDOMINATION Summary of this function goes here
%   Detailed explanation goes here

for p = Pop
    p.dominated = false;
end

for i = 1:popsize-1
    for j = i+1:popsize
        if Dominates(Pop(i).objs_vals, Pop(j).objs_vals, pb.objectives_nb)
            Pop(j).dominated = true;
        end
        
        if Dominates(Pop(j).objs_vals, Pop(i).objs_vals, pb.objectives_nb)
            Pop(i).dominated = true;
        end
    end
end
end

