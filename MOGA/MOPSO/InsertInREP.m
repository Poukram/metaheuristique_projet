function REP = InsertInREP(REP, ti, pb)
%MANAGEREP Summary of this function goes here
%   Detailed explanation goes here
tokeep(numel(REP)) = true;
ok4ins(numel(ti)) = true;

% We remove all dominated elements and insert all non dominated ones
for i = 1:numel(ti)
   for r = 1:numel(REP)
       if Dominates(ti(i).objs_vals, REP(r).objs_vals, pb.objectives_nb)
          tokeep(r) = false; 
       end
       if Dominates(REP(r).objs_vals, ti(i).objs_vals, pb.objectives_nb)
          ok4ins(i) = false; 
       end
   end
end

REP = REP(tokeep);
REP = [REP ti(ok4ins)];
end

