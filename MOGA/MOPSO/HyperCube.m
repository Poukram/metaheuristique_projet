classdef HyperCube < matlab.mixin.Copyable
    %HYPERCUBE Summary of this class goes here
    %   Detailed explanation goes here
    
    properties
        bounds;
        numpts;
        indcnt;
        fitness;
    end
    
    methods
        function obj = HyperCube(numdim, numdiv)
            %HYPERCUBE Construct an instance of this class
            %   Detailed explanation goes here
            if (nargin == 0)
               numdim = 0;
               numdiv = 0;
            end
            obj.bounds = zeros(numdim, 2, numdiv + 1);
            obj.numpts = numdiv + 1;
            obj.fitness = zeros(1, (obj.numpts)^numdim);
            obj.indcnt = zeros(1, (obj.numpts)^numdim);
        end
    end
    methods (Static)
        function HCVec = GetNewVect(numdim, numhc)
            HCVec(numhc) = HyperCube;
            for i = 1:numhc
                HCVec(i) = HyperCube(numdim);
            end
        end
    end
end

