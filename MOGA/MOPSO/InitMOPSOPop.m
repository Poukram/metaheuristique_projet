function Pop = InitMOPSOPop(popsize, pb)
%INITMOPSOPOP Summary of this function goes here
%   Detailed explanation goes here
Pop = MOPSO_Ind.GetNewVect(popsize, pb.vars_nb, pb.objectives_nb);

for n = 1:popsize
    Pop(n).vars = pb.GenRndVars();
    Pop(n).speed = zeros(1, pb.vars_nb);
end

