function b_ind = SelBIndInHCubes(REP, HCubes)
%SELBINDINHCUBES Summary of this function goes here
%   Detailed explanation goes here
hcidx = find(HCubes.fitness > 0);
totfit = sum(HCubes.fitness(hcidx));
selprob = zeros(1, numel(hcidx));
prevsel = 0;

for i = 1:numel(hcidx)
    selprob(i) = HCubes.fitness(hcidx(i))/totfit + prevsel;
    prevsel = selprob(i);
end

tmp = rand();
idx = 0;

for i = 1:numel(selprob)
    tmp = tmp - selprob(i);
    if (tmp < 0)
        idx = i;
        break;
    end
end

if (idx == 0)
    idx = numel(selprob);
end

idx = hcidx(idx); % the index of the hypercube to use to find a particle

tmp = REP([REP.hcidx] == idx); % we take all particles in this HC
sel =  randsample(numel(tmp),1);

b_ind = tmp(sel);
end

