function res = MOPSO(param)
%MOGMA3 Summary of this function goes here
%   Detailed explanation goes here
    
    res = cell(1,param.g_max);
    P = InitMOPSOPop(param.popsize, param.pb);
    REP = Archive(param.archsize, param.pb, param.nbdiv);
    EvalPop(P, param.pb);
    EvalPopDomination(P, param.popsize, param.pb);
    
    for p = P
        p.bvars = p.vars;
        p.bobjsvals = p.objs_vals;
    end
    
    for p = P(~[P.dominated])
        REP.addsol(copy(p));
    end
        
    for i = 1:param.g_max
        pm = (1-((i-1)/param.g_max))^(5/param.pm); % proba of mutating
        % We want to decrease exploration over time
        W = 1.2 - 0.8 * (i/param.g_max);
        for p = P
            % We choose a leader in the best knnown individuals
            [~, b_ind_idx] = REP.rndSolFromDensestCell();
            b_ind = REP.solutions(b_ind_idx);
            % Speed is computed according to previous speed, internal
            % memory and group memory
            R1 = rand();
            R2 = rand();
            C1 = rand() * 0.5 + 1.5;
            C2 = rand() * 0.5 + 1.5;
            for s = 1:numel(p.speed)
                p.speed(s) = W * p.speed(s) + (R1 * C1 * (p.bvars(s) - p.vars(s))) +...
                    (R2 * C2 * (b_ind.vars(s) - p.vars(s)));
            end
            % The position is updated
            p.vars = p.vars + p.speed;
            
            % We can mutate the individual along a random dimension

            MutateInd(p, pm, param.pb); 
            
            % we refelct speed along the dimensions where we went beyond
            % the problem's bound (and stick the particle on the boundary)
            BoundsCrossCorrect(p, param.pb);
            EvalPop(p, param.pb);
    
            % Update the particle's memory of its best know position
            if (Dominates(p.objs_vals, p.bobjsvals, param.pb.objectives_nb))
                p.bvars = p.vars;
                p.bobjsvals = p.objs_vals;
            elseif (Dominates(p.bobjsvals, p.objs_vals, param.pb.objectives_nb))
                continue;
            else
                % If neither position dominates the other one of them is
                % randomly chosen to be momorized
                if rand() < 0.5
                   p.bvars = p.vars;
                   p.bobjsvals = p.objs_vals;
                end
            end
        end
        
        EvalPopDomination(P, param.popsize, param.pb);
        for p = P(~[P.dominated])
            REP.addsol(copy(p));
        end
        res{i} = REP.solutions;
    end
end

