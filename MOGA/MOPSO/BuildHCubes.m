function HCubes = BuildHCubes(REP, nb_div)
%BUILDHCUBES Summary of this function goes here
%   Detailed explanation goes here
expand_factor = 0.1; % 10 percent wider than the actual bounding box
vals = vertcat(REP.objs_vals);
numdim = size(vals, 2);

minval = min(vals,[],1);
maxval = max(vals,[],1);

ranges = maxval - minval;

hcminval = minval - expand_factor * ranges;
hcmaxval = maxval + expand_factor * ranges;

HCubes = HyperCube(numdim, nb_div);

for i = 1:numdim
    eqdiv = linspace(hcminval(i), hcmaxval(i), nb_div+2);
    HCubes.bounds(i, 1, :) = eqdiv(1:end-1);
    HCubes.bounds(i, 2, :) = eqdiv(2:end);
end
end

