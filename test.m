%------------Configuring the path and initializing -------------------
clear variables
close all
clc
addpath(genpath(pwd))

pb_names = ["FON", "KUR", "POL", "SCH", "ZDT1", "ZDT2", "ZDT3", "ZDT4", "ZDT6"];
algo_names = ["MOPSO", "PAES"];
%---------------------------------------------------------------------

param.popsize = 100;
param.archsize = 100;
param.g_max = 120; % The maximum number of generations
param.pc = 0.6; % The probability of a crossover to happen
param.pm = 0.5; % The probability of a mutation to happen
param.nbdiv = 30;

%------------Call to the genetic algorithm-----------

param.pb = GenericProblem(pb_names(3));


param.algo_name = algo_names(1);
res1 = MOGACaller(param);


param.algo_name = algo_names(2);
param.g_max = 10000;
res2 = MOGACaller(param);


figure('Name',algo_names(1) + ' pareto front','NumberTitle','off');
res = res1{end};
rfov = vertcat(res.objs_vals);
x = rfov(:,1);
y = rfov(:,2);
scatter(x,y)


figure('Name',algo_names(2) + ' pareto front','NumberTitle','off');
res = res2{end};
rfov = vertcat(res.objs_vals);
x = rfov(:,1);
y = rfov(:,2);
scatter(x,y)
