function zdt3(Pop)
%ZDT3 Returns ZDT3's two objective functions
%   functions returned as an array of function handles
    function res = g(t)
        res = 1 + 9 * sum(t(2:end))/29; %(n - 1) = 29 because in ZDT1 n = 30
    end

for i = 1:numel(Pop)
    x = Pop(i).vars;
    
    Pop(i).objs_vals(1) = x(1);    
    Pop(i).objs_vals(2) = g(x)*(1-sqrt(x(1)/g(x) - x(1)*sin(10*pi*x(1))/g(x)));
end
end

