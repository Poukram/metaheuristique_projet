function pol(Pop)
%POL Returns POL's two objective functions
%   functions returned as an array of function handles
A1 = sin(1)/2 - 2*cos(1) + sin(2) - 3*cos(2)/2;
A2 = 3*sin(1)/2 - cos(1) + 2*sin(2) - cos(2)/2;

for i = 1:numel(Pop)
    B1 = sin(Pop(i).vars(1))/2 - 2*cos(Pop(i).vars(1)) + sin(Pop(i).vars(2)) - 3*cos(Pop(i).vars(2))/2;
    B2 = 3*sin(Pop(i).vars(1)) - cos(Pop(i).vars(1)) + 2*sin(Pop(i).vars(2)) - cos(Pop(i).vars(2))/2;

    Pop(i).objs_vals(1) = 1 + (A1 - B1)^2 + (A2 - B2)^2;   
    Pop(i).objs_vals(2) = (Pop(i).vars(1) + 3)^2 + (Pop(i).vars(2) + 1)^2;
end
end

