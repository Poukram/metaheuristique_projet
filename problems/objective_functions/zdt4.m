function zdt4(Pop)
%ZDT4 Returns ZDT4's two objective functions
%   functions returned as an array of function handles
    function res = g(t)
        t = t(2:end);
        res = 91 + sum(t.^2 - 10 * cos(4*pi*t)); %(n - 1) = 29 because in ZDT1 n = 30
    end

for i = 1:numel(Pop)
    x = Pop(i).vars;

    Pop(i).objs_vals(1) = x(1);
    Pop(i).objs_vals(2) = g(x)*(1-sqrt(x(1)/g(x)));
end
end

