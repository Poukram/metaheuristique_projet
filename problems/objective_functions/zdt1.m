function zdt1(Pop)
%ZDT1 Returns ZDT1's two objective functions
%   functions returned as an array of function handles
    function res = g(t)
        res = 1 + 9 * sum(t(2:end))/29; %(n - 1) = 29 because in ZDT1 n = 30
    end

for i = 1:numel(Pop)
    Pop(i).objs_vals(1) = Pop(i).vars(1);
    Pop(i).objs_vals(2) = g(Pop(i).vars)*(1-sqrt(Pop(i).vars(1)/g(Pop(i).vars)));
end
end

