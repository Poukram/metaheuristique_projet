function kur(Pop)
%KUR Returns KUR's two objective functions
%   functions returned as an array of function handles
    function res = f1_sum(t)
        res = 0;
        for n = 1:2
            res = res -10*exp(-0.2 *sqrt(t(n)^2 + t(n+1)^2));
        end
    end

for i = 1:numel(Pop)
    Pop(i).objs_vals(1) = f1_sum(Pop(i).vars);
    Pop(i).objs_vals(2) = sum(abs(Pop(i).vars).^0.8 + 5*sin(Pop(i).vars.^3));
end
end
