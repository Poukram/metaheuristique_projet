function fon(Pop)
%FON Returns FON's two objective functions
%  functions returned as an array of function handles

for i = 1:numel(Pop)
    Pop(i).objs_vals(1) = 1 - exp(-sum((Pop(i).vars - 1/sqrt(3)).^2));
    Pop(i).objs_vals(2) = 1 - exp(-sum((Pop(i).vars + 1/sqrt(3)).^2)); 
end
end