function sch(Pop)
%SCH Returns SCH's two objective functions
%   functions returned as an array of function handles

for i = 1:numel(Pop)
    Pop(i).objs_vals(1) = Pop(i).vars^2;
    Pop(i).objs_vals(2) = (Pop(i).vars - 2)^2;
end
end

