function zdt6(Pop)
%ZDT6 Returns ZDT6's two objective functions
%   functions returned as an array of function handles
    function res = g(t)
        %(n - 1) = 9 because in ZDT6 n = 10
        res = 1 + 9 * (sum(t(2:end))/9)^0.25;
    end

for i = 1:numel(Pop)
    x = Pop(i).vars;

    Pop(i).objs_vals(1) = 1- exp(-4*x(1))*sin(6*pi*x(1))^6;
    Pop(i).objs_vals(2) = g(x)*(1-(Pop(i).objs_vals(1)/g(x))^2);
end
end

